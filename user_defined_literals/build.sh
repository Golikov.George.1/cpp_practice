#!/bin/bash
# Прервемся и выйдем аварийно по ошибке
set -e

echo Компилируем sea
g++ -o sea -std=c++14 -Wall -Wextra -Wold-style-cast sea.cpp

echo Запускаем sea
./sea

#exit 0

echo Компилируем cards
g++ -o cards -std=c++14 -Wall -Wextra -Wold-style-cast cards.cpp

echo Запускаем cards
./cards