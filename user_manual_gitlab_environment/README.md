Руководство по установке git окружения и работе с GitLab
========================================================
  - [Руководство по установке git окружения и работе с GitLab](#Руководство-по-установке-git-окружения-и-работе-с-gitlab)
- [Введение](#Введение)
- [Установка git](#Установка-git)
  - [**Windows**:](#windows)
  - [**Linux**:](#linux)
  - [**MacOS**:](#macos)
- [Настройка git](#Настройка-git)
- [Работа с GitLab](#Работа-с-gitlab)
  - [Введение](#Введение-1)
  - [Регистрация на GitLab](#Регистрация-на-gitlab)
  - [SSH ключи](#ssh-ключи)
    - [ **Windows**](# windows)
    - [**Linux** и **MacOS**](#linux-и-macos)
    - [**Все системы**](#Все-системы)
  - [Получение локальной копии проекта (_Fork/Clone_).](#Получение-локальной-копии-проекта-forkclone)
    - [**Все системы**](#Все-системы-1)
    - [**Windows**](#windows-1)
    - [**Linux** и **MacOS**](#linux-и-macos-1)
    - [**Все системы**](#Все-системы-2)
  - [Координация с общим репозиторием проекта](#Координация-с-общим-репозиторием-проекта)
    - [**Все системы**](#Все-системы-3)
    - [**Windows**](#windows-2)
    - [**Linux**  и **MacOS**:](#linux- и-macos)
  - [Внесение изменений в твою копию проекта](#Внесение-изменений-в-твою-копию-проекта)
    - [**Windows**](#windows-3)
    - [**Linux**  и **MacOS**](#linux- и-macos-1)
    - [**Все системы**](#Все-системы-4)
  - [Запрос на слияние кода (Merge Request)](#Запрос-на-слияние-кода-merge-request)
    - [Все системы](#Все-системы-5)
- [Установка компилятора](#Установка-компилятора)
  - [**Windows**](#windows-4)
  - [**Linux**](#linux-1)
  - [**MacOS**](#macos-1)
- [Boost](#boost)
  - [**Windows**](#windows-5)
  - [**Linux**](#linux-2)
  - [**MacOS**](#macos-2)
- [Контакт для вопросов и комментариев](#Контакт-для-вопросов-и-комментариев)

# Введение
В руководстве описана установка git окружения в операционных системах Windows, Linux и MacOS; и работа с  GitLab на примере данного проекта. Работа с GitLab для проведения лабораторных работ  проводится аналогично - просто другой проект, адрес которого будет сообщен дополнительно. Данный проект позволяет практиковаться в GitLab для уверенного использования GitLab для лабораторных.

# Установка git
## **Windows**:
* По этой ссылке <https://notepad-plus-plus.org/download/v7.6.3.html> загрузи и установи редактор _notepad++_. Он будет использован для работы с _git_, но также может быть использован для разработки кода, в том числе и лабораторных работ.
* По этой ссылке <https://git-scm.com/download/win> загрузи инсталлятор _git bash_. Выполни установку, **приняв все опции по умолчанию**. В качестве редактора укажи _notepad++_.
* После установки запусти _Git Bash_ из меню _Start_. Появится окно командной строки (терминала) с возможностью выполнять команду _git_ и другие команды Unix-like систем. Например, _ls_ для показа файлов в каталоге. В этом терминала используются прямые слеши (**/**), а не обратные (**\\**). Доступ к файлам Windows осуществляется так: _/с/Users/_.

## **Linux**: 
Установи _git_ пакетным менеджером твоего дистрибутива Linux. Например, для Debian (Ubuntu, Mint) систем установка будет выполнена командой, поданной из терминала:
> sudo apt install git.

## **MacOS**:
Установи из _AppStore_ _XCode_, который включает _git_.

# Настройка git
В терминале твоей операционной системы выполни команды:
> git config --global user.name "Твое имя"

> git config --global user.email "Твой email адрес"

Эти настройки позволят идентифицировать твои изменения в общей базе _git_, которая называется **репозиторий**. Общий репозиторий будет храниться на сервере <https://gitlab.com>. Ты будешь работать с локальной копией репозитория на твоем компьютере и загружать свои изменения в общий репозиторий с помощью **merge request**.

_Примечание: в русскоязычном сообществе почти все понятия git являются английскими словами. Например, слияние изменений называется **мерж**. Это устоявшаяся практика. Однако, существует полная документация по git на русском языке: <https://git-scm.com/book/ru/v1/Введение-Основы-Git>. Следует обращаться к ней по мере необходимости_.

# Работа с GitLab
## Введение
 GitLab (<https://gitlab.com>) - это общедоступный веб-сайт для совместной работы над проектами. В частности, данный документ является частью проекта. Лабораторные работы будут другим проектом. GitLab доступен как отдельный продукт в свободной и коммерческой (расширенной) версиях. Многие компании используют этот продукт для поддержки собственной кодовой базы. Ты можешь скачать и установить GitLab на свой компьютер, но этот предмет находится за пределами данного руководства. 

## Регистрация на GitLab
 Если у тебя еще нет аккаунта на GitLab, создай его на <https://gitlab.com>. Помести на аватар свою фотографию, напиши немного о себе по-русски или по-английски. сообщи свой девиз - **заяви о себе**, сделай себя интересным для других людей. 

## SSH ключи
 Для работы с GitLab рекомендуется использовать доступ к репозиторию по **SSH**. SSH (<https://ru.wikipedia.org/wiki/SSH>) обеспечивает безопасное шифрованное соединение между пользователями с помощью ключей. Для обеспечения соединения с GitLab ты должен добавить в свой аккаунт на GitLab открытый ключ твоего компьютера. Ты можешь использовать много компьютеров для работы со своим аккаунтом, но для каждого ты должен указать ключ компьютера. Таким образом, в твоем аккаунте может быть несколько ключей соответственно компьютерам, которые ты используешь для доступа. 

 Для генерации ключей используется утилита  **ssh-keygen**.

###  **Windows**
 В Git Bash выполни команду 
 
 > ssh-keygen.exe 

 Ответь на все вопросы по умолчанию. Введи команду:

 > find ~/ -name id_rsa.pub

 Эта команда выведет путь к твоему файлу открытого ключа. Открой файл с помощью редактора _notepad++_. Файл должен начинаться с последовательности символов _ssh-rsa_ и далее длинный набор символов ключа. Выбери и скопируй (_copy_) все содержимое файла.

### **Linux** и **MacOS**
 В терминале выполни команду

 >  ssh-keygen

 Ответь на все вопросы по умолчанию. 

 Выполни команду:
> cat ~/.ssh/id_rsa.pub

На экран выведется твой открытый ключ, начинающийся с последовательности символов _ssh-rsa_ и далее длинный набор символов ключа. В терминале выбери всю последовательность и скопируй ее (_copy_). 


### **Все системы**
 Зайди в свой аккаунт на GitLab и в левой вертикальной полосе иконок кликни на знак ключа _SSH Keys_. Вставь (_paste_) содержимое буфера твоей операционной системы, в котором находится открытый ключ, в поле **Key**, проверь правильность данных и кликни на кнопку **Add key**.

 Теперь ты можешь работать с проектами GitLab по _ssh_. 

## Получение локальной копии проекта (_Fork/Clone_).

### **Все системы**
 1. Зайди на страницу данного проекта: <https://gitlab.com/alexeit/cpp_practice>
 2. Нажми на кнопку **Fork**.
 3. GitLab создаст копию проекта для твоего аккаунта.
 4. Нажми кнопку **Clone**.
 5. В выпавшем меню выбери строчку, написанную в окошке **Clone with SSH** (_ОБЯЗАТЕЛЬНО_). Скопируй содержимое строки вида: git@gitlab.com:ТВОЙ_НИК/cpp_practice.git 

### **Windows**
 В терминале _Git Bash_ выполни команду:

 > git clone git@gitlab.com:ТВОЙ_НИК/cpp_practice.git

### **Linux** и **MacOS**
 В терминале выполни команду: 

 > git clone git@gitlab.com:ТВОЙ_НИК/cpp_practice.git

### **Все системы**
 В каталоге _cpp_practice_ ты получишь свою копию проекта.

## Координация с общим репозиторием проекта

### **Все системы**
 Войди в каталог _cpp_practice_, выполнив команду:

 > cd cpp_practice

### **Windows**
 В _Git Bash_, находясь в каталоге _cpp_practice_, выполни команду:

 > git remote add main git@gitlab.com:alexeit/cpp_practice.git

### **Linux**  и **MacOS**: 
 В терминале, находясь в каталоге _cpp_practice_, выполни команду:

 > git remote add main git@gitlab.com:alexeit/cpp_practice.git

## Внесение изменений в твою копию проекта

 Ты можешь внести изменения в любой файл проекта, если думаешь, что этим улучшишь его. Автор проекта рассмотрит твои изменения и либо примет, либо отвергнет их. 

 Однако, лучше работай в своем отдельном каталоге твоей копии проекта. Выбери название каталога и создай его.

### **Windows**
В _Git Bash_, находясь в каталоге _cpp_practice_, выполни команды:

 > mkdir НАЗВАНИЕ_ТВОЕГО_КАТАЛОГА

 > cd НАЗВАНИЕ_ТВОЕГО_КАТАЛОГА

### **Linux**  и **MacOS**

В терминале, находясь в каталоге _cpp_practice_, выполни команды:

 > mkdir НАЗВАНИЕ_ТВОЕГО_КАТАЛОГА

 > cd НАЗВАНИЕ_ТВОЕГО_КАТАЛОГА

### **Все системы**
 1. С помощью любого редактора (например, **Windows** _notepad++) создай текстовый файл - желательно, чтобы это был код на C++. Назови файл _main.cpp_.
 2. В терминале (**Windows** _Git Bash_, в **Linux** и **MacOS** в терминале) введи команды:

 > git add main.cpp

 > git commit -m "Начальная версия"

 > git push

## Запрос на слияние кода (Merge Request)
 Теперь нужно внести изменения, которые ты сделал в своей локальной копии, в общий репозиторий проекта. На предыдущем шаге ты сделал _git push_ и изменения на твоем компьютере загрузились в твою копию проекта на GitLab. Теперь нужно сделать запрос на слияние кода в общий репозиторий.

### Все системы  
 1. Зайди на страницу общего проекта:  <https://gitlab.com/alexeit/cpp_practice>
 2. Нажми на кнопку **New Merge Request**
 3. В предложенных окнах выбери ветки _master_ и нажми кнопку **Compare...**
 4. На появившейся страницу в поле **Title** напиши краткое описание запроса
 5. Нажми кнопку **Submit Merge Request**
 6. Автор проекта получит уведомление по электронной почте о твоем запросе, посмотрит его и выполнит слияние в общий проект, так называемый _мерж_.
7. Для получения актуальной копии проекта выполни команду:

> git pull

# Установка компилятора
Данный проект и лабораторные работы предполагают компиляцию с помошью компилятора _GNU g++_ и совместимых с ним. Желательно установить компилятор для возможности предварительной проверки лабораторных перед выполнением Merge Request.

## **Windows**
Для установки компилятора C++, совместимого с _GNU g++_, установи MinGW Toolkit. Действуй по этой ссылке: <https://yichaoou.github.io/tutorials/software/2016/06/28/git-bash-install-gcc>

## **Linux**
Компилятор уже может быть установлен в систему. Проверть это, выполнив в терминале команду:

> g++ -v

Если компилятор не найден, установи его с помощью менеджера пакетов твоего дистрибутива. Например, для Debian (Ubuntu, Mint) выполни команду:

> sudo apt install g++

## **MacOS** 
Поскольку _Xcode_ установлен, компилятор уже есть в системе. 

# Boost
**Boost** - это большая библиотека для решения различных задач на C++. В курсе лабораторных она будет применяться для автоматизированного тестирования. Особенность _boost_ является то, что очень большая ее часть содержится в заголовочных файлах (header).

Можно отложить установку _boost_ до времени, когда ты выполнишь первые лабораторные и освоишься в _git_ окружении.

## **Windows**
Установка boost в Windows - не совсем тривиальная задача. Действуй по ссылке <https://www.boost.org/doc/libs/1_55_0/more/getting_started/windows.html#the-boost-distribution> или попробуй найти руководство на родном языке. Эта установка будет работать для разработки под Visual Studio. 

Для установки _boost_ в _Git Bash_ обратись к информации, подобной этой: <https://stackoverflow.com/questions/20265879/how-do-i-build-boost-1-55-with-mingw>

## **Linux**
В Linux _boost_ устанавливается пакетным менеджером. Например, для Debian (Ubuntu, Mint) выполни команду:

> sudo apt install boost

## **MacOS**
В MacOS _boost_ устанавливается как пакет, портированный из Linux. Это делается с помощью **brew**. Официальное руководство от _Apple_ по этой ссылке: <http://macappstore.org/boost/>

# Контакт для вопросов и комментариев
Если не получается или неясно или есть замечания и комментарии, напиши <alexey.tchervinsky@gmail.com>. Можно комментировать этот файл на <https://gitlab.com>

