// pointer, references, functions

#include <iostream>

using namespace std;

struct s_t {
	int i;
	float f;
	struct {
		double d;
	} s;
};



void val(int i)
{
	i = 1;
}

void ptr(int *i)
{
	*i = 1;
}

void ref(int & i)
{
	i = 2;
}

void ref_s(s_t *st)
{
	st->i = 101;
	st->s.d = 0.2;
}

int main()
{
	int k = 0;

	val(k);
	cout << k << endl;

	ptr(&k); 
	cout << k << endl;


	ref(k); 
	cout << k << endl;

	s_t st;

	ref_s(&st);
	cout << st.i << endl;
	cout << st.s.d << endl;
	
	struct {
		double d;
    } an;

	an.d =0.1; 
	cout << an.d << endl;

	return 0;	
}